import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-component',
  templateUrl: './new-component.component.html',
  styleUrls: ['./new-component.component.css']
})
export class NewComponentComponent implements OnInit {
  myname = 'mynamethis';
  firstName :string = '';
  lastName :string = '';
  constructor() { }

  ngOnInit() {
  }

  updates () {
    console.log('hi' + this.firstName);
  }

}
